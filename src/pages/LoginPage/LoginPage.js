import React, { useState } from "react";
import { auth } from "../../firebaseConfig";
import {
  signInWithEmailAndPassword,
  sendPasswordResetEmail,
} from "firebase/auth";
import Input from "../../components/Input";
import { MdEmail, MdLock, MdPersonAdd } from "react-icons/md";
import "./LoginPage.css";
import { useNavigate } from "react-router-dom";
import { getProfessor } from "../../api/professorApi";
import { getAluno } from "../../api/alunoApi";


function LoginPage() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const navigate = useNavigate();


  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const userCredential = await signInWithEmailAndPassword(auth, email, password);
      const uid = userCredential.user.uid;

      // Verifique se o UID existe na coleção de professores
      const professorOuAluno = await getProfessor(uid);

      if (professorOuAluno.exists) {//verificamos se é professor
        navigate("/coordenacao-home");
      } else {//depois verificamos se é aluno
        professorOuAluno = await getAluno(uid);
        if (professorOuAluno.exists)
          navigate("/aluno-home");
      }
      //o caso ideal aqui seria criar uma collection Usuarios(alunos e professores)

    } catch (err) {
      setError(err.message);
    }
  };


  const handleForgotPassword = async () => {
    try {
      await sendPasswordResetEmail(auth, email);
      alert("Verifique seu e-mail para redefinir a senha.");
    } catch (err) {
      setError(err.message);
    }
  };

  return (
    <div className="login-home">
      <div className="login-container">
        <h2>Login</h2>
        <form onSubmit={handleSubmit}>
          <Input
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            icon={<MdEmail size={20} color="#168e3c" />}
            placeholder="Email"
          />
          <Input
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            icon={<MdLock size={20} color="#168e3c" />}
            placeholder="Password"
          />
          <button className="login-button" type="submit">Login</button>
        </form>
        {error && <p className="error-message">{error}</p>}

        <div className="signup-forgot">
          <button className="forgot-button" onClick={handleForgotPassword}>
            Esqueci a Senha
          </button>
        </div>
      </div>
    </div>
  );
}

export default LoginPage;
